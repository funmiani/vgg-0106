const whatEver = document.getElementById('whatEver'); 
const commentRugged = document.getElementById('commentRugged');
const postUrl = 'https://jsonplaceholder.typicode.com/posts?_start=&_limit=5'; // The Api link for post blog
const callComment = 'https://jsonplaceholder.typicode.com/comments?postId=1';   //The Api link for post comment


function postAny() {
    fetch(postUrl)
    .then(response => response.json())    
    .then(json =>{
        json.map(value=>{
        whatEver.innerHTML+= `
        <div class="innerHead" >
          <div class="innerHeadOne">
          <div class="innerContent">
            <div>
                <h2>${value.title}</h2>
                <p>Paul Bolton <a href="#">Design</a> April 9, 2019</p>
            </div>
            <div>
                <img src="img/blog-img.jpg" alt="" srcset="">
            </div>
            <div>
            <p>${value.body}</p>
              <button id="readMore" class=" btn btn-primary " onClick="postComment()">Read more</button>
            </div> 
            </div>
            </div>
        </div>
         `
    })
 } ); 

}
postAny();

function renderComment() {
    fetch(callComment)
        .then(response => response.json())
        .then(json => {
            json.map(value => {
                commentRugged.innerHTML += `
        <div class="boxCommment">
       <h4 class="small-title">Comments</h4>
     <div class="row">
        <div class="col-lg-12">
            <div class="comments-list">
                <div class="media">
                    <a class="media-left" href="#">
                        <img src="upload/author.jpg" alt="" class="rounded-circle">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading user_name">${value.name}</h4>
                        <p>${value.email}</p>
                        <p>${value.body}</p>
                        <a href="#" class="btn btn-primary btn-sm">Reply</a>
                    </div>
        </div> 
             `
    })
 } ); 

}
renderComment();


function postComment() {
    
    window.location.href ="BlogRead.html";
    
};

